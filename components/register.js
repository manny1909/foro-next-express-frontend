import {useState} from 'react'
import axios from 'axios'
export default function register(props) {
    const [nombre,setNombre]=useState('')
    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const cleanState=()=>{
        setNombre('')
        setEmail('')
        setPassword('')
    }
    const handleSubmit=async e=>{
        e.preventDefault()
        const res= await axios.post('http://localhost:3000/api/user/add',{nombre,email,password})
        console.log(res.status);
        cleanState()
    }
    return (
        <div className="d-flex justify-content-center">
            <div className="card bg-secondary">
            <div className="card-header">
                <h2 className="card-title text-success text-monospace">REGISTRARSE</h2>
            </div>
            <div className="card-body text-center">
                <form onSubmit={handleSubmit}>
                <div className="form-group">
                <label className="h5 font-weight-lighter">name</label>    
                <input type="text"  className="form-control" name="nombre" value={nombre} onChange={e=>{setNombre(e.target.value)}}></input>
                    </div>
                    <div className="form-group">
                <label className="h5 font-weight-lighter">Email</label>    
                <input type="text"  className="form-control" name="email" value={email} onChange={e=>{setEmail(e.target.value)}}></input>
                    </div>
                    <div className="form-group">
                <label className="h5 font-weight-lighter">password</label>    
                <input type="password"  className="form-control" name="password" value={password} onChange={e=>{setPassword(e.target.value)}}></input>
                    </div>
                    <button className="btn btn-light btn-block" type="submit"> enviar</button>
                    </form>    
            </div>
            </div>
        </div>
    )
}
