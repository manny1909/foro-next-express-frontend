import axios from "axios"
import { useState, useEffect } from 'react'
import { useRouter } from "next/router"
import Cookies from 'universal-cookie'
const cookies = new Cookies()
export default function Login(props) {
  const [token, setToken] = useState('')
  useEffect(() => {
    const a = () => {
      setToken(localStorage.getItem('token') ? localStorage.getItem('token') : '')
    }
    a()
  })
  const router = useRouter()
  const auth = async (e) => {
    e.preventDefault()
    const email = e.target.email.value
    const password = e.target.password.value
    try {
      console.log('email y password: ' + email + password);
      const res = await axios.post('http://localhost:3000/api/auth', {
        email, password
      }
      )
      setToken(res.data)
      localStorage.setItem('token', token)
      return router.push('http://localhost:4000/u/all')
    } catch (error) {
      console.error(error);
    }
  }
  return (
    <div>
      <div className="text-center">
        {/* Button HTML (to Trigger Modal) */}
        <a href="#login" className="nav-link" data-toggle="modal">iniciar sesion</a>
      </div>
      {/* Modal HTML */}
      <div id="login" className="modal fade">
        <div className="modal-dialog modal-login">
          <div className="modal-content">
            <form onSubmit={auth}>
              <div className="modal-header">
                <h4 className="modal-title">Login </h4>
                <button id="closeModal" type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label>Correo electronico</label>
                  <input name="email" type="text" className="form-control" required="required" />
                </div>
                <div className="form-group">
                  <div className="clearfix">
                    <label>clave</label>
                    <a href="#" className="float-right text-muted"><small>olvido su clave?</small></a>
                  </div>
                  <input name="password" type="password" className="form-control" required="required" />
                </div>
              </div>
              <div className="modal-footer justify-content-between">
                <label className="form-check-label"><input type="checkbox" /> recuerdame</label>
                <input type="submit" className="btn btn-primary" defaultValue="Login" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
