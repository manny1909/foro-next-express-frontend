import React from 'react'

function AddPost() {
    const [titulo,setTitutlo]=useState('')
    const [parrafo,setParrafo]=useState('')
    const cleanState=()=>{
        setTitutlo('')
        setParrafo('')
    }
    const handleSubmit=async e=>{
        e.preventDefault()
        const res= await axios.post('http://localhost:3000/api/post/add',{parrafo,id_user_fk,titulo})
        console.log(res.status);
        cleanState()
    }
    return (
        <div>
            <div className="card bg-secondary">
            <form onSubmit={handleSubmit}>    
            <div className="card-header">
            <div className="form-group">
                <label className="h5 font-weight-lighter">titulo</label>    
                <input type="text"  className="form-control" name="titulo" value={titulo} onChange={e=>{setTitulo(e.target.value)}}></input>
                    </div>
            </div>
            <div className="card-body text-center">
                <div className="form-group">
                <textarea type="text"  className="form-control" name="parrafo" value={nombre} onChange={e=>{setParrafo(e.target.value)}} placeholder="escribe la publicacion aqui"></textarea>
                    </div>
                    <button className="btn btn-light btn-block" type="submit"> publicar</button>
            </div>
            </form>    
            </div>
        </div>
    )
}

export default AddPost
