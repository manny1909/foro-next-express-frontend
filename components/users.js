import User from './user'
const Users=(props)=>{
    return(
        <ul>
            {
                props.users.map(user=>(
                   <li key={user.id}>
                       <User user={user}/>
                   </li>  
                 )
                 )
            }
        </ul>
    )
}
export default Users;