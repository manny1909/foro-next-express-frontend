import Post from './post'

const Posts=(props)=>{
    return (
        <div>
            <h2 className="text-center"> publicaciones</h2>
            <br/>
            {   
                props.posts.map(post=>(
                    <div key={post.id}>
                        <Post post={post}/>
                        <br/>
                    </div>
                ))
            }
        </div>
    )
}


export default Posts
