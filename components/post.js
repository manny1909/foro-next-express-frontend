import Comments from './comments'
import {useState} from 'react'
import axios from 'axios'
const Post =(props)=>{
    const [comentar,setComentar]=useState(false)
    const [comentario,setComentario]=useState('')
    const expandComment=e=>{
        setComentar(!comentar)   
    }
    const sendComment=async e=>{
        const response=await axios.post('http://localhost:3000/api/comment/add',{
            parrafo:comentario,
            id_user_fk:req.userId,
            destinatario:'',
            id_post_fk:props.post.id
        })
        console.log(response.data);
    }
    return (
            <div className="card" key={props.post.id}>
                <div className="card-header">
                    <h3>{props.post.titulo}</h3>
                    <br></br> 
                    <h5>por: {props.post.id_user_fk}</h5>
                </div>
                <div className="card-body text-dark bg-light">
                        <p className="">{props.post.parrafo}</p>
                </div>
                <div className="card-footer">
                    <button className="btn btn-outline-dark border-0" onClick={expandComment}> comentar</button>                    
                </div>
                <div id="comments" className="text-center">
                    <div className="text-dark bg-light">
                    <Comments post={props.post.id} key={props.post.id}/>
                    </div>
                    {comentar?<div id="comment" className="input-group">
                        <input type="text" className="form-control" onChange={e=>{setComentario(e.target.value)}}></input>
                        <button className="btn btn-primary"
                        value={comentario} onClick={sendComment}>enviar</button>
                    </div>:''}                    
                </div>
            </div>
    )
}
export default Post;