import {useEffect,useState} from 'react'
import axios from 'axios'
import Comment from './comment'

const Comments=(props)=>{
    const [ListComments,setListComments]=useState(null)
    const getComments=async()=>{
        const response= await axios.get('http://localhost:3000/api/comments/'+props.post)
        const res=response.data
        return res
    }
    useEffect(async()=>{
       const res=await getComments()
      setListComments(res)
    },[])    
    return(
        <div>
           {ListComments!=null?
            ListComments.map(comment=>(
                <Comment comment={comment} key={comment.id}></Comment>
            )):''   
        }
        </div>
    )
}



export default Comments
