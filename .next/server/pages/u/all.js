/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/u/all";
exports.ids = ["pages/u/all"];
exports.modules = {

/***/ "./components/user.js":
/*!****************************!*\
  !*** ./components/user.js ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n\nvar _jsxFileName = \"/home/manny/Escritorio/foro-next-express-frontend/components/user.js\";\n\nconst User = props => {\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h5\", {\n      children: props.user.nombre\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 3,\n      columnNumber: 9\n    }, undefined)\n  }, props.user.id, false, {\n    fileName: _jsxFileName,\n    lineNumber: 2,\n    columnNumber: 12\n  }, undefined);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (User);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mb3JvLW5leHQtZXhwcmVzcy1mcm9udGVuZC8uL2NvbXBvbmVudHMvdXNlci5qcz9jMGE0Il0sIm5hbWVzIjpbIlVzZXIiLCJwcm9wcyIsInVzZXIiLCJub21icmUiLCJpZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsTUFBTUEsSUFBSSxHQUFFQyxLQUFELElBQVM7QUFDaEIsc0JBQU87QUFBQSwyQkFDSDtBQUFBLGdCQUNLQSxLQUFLLENBQUNDLElBQU4sQ0FBV0M7QUFEaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURHLEtBQVVGLEtBQUssQ0FBQ0MsSUFBTixDQUFXRSxFQUFyQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBQVA7QUFLSCxDQU5EOztBQU9BLCtEQUFlSixJQUFmIiwiZmlsZSI6Ii4vY29tcG9uZW50cy91c2VyLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgVXNlcj0ocHJvcHMpPT57XG4gICAgcmV0dXJuKDxkaXYga2V5PXtwcm9wcy51c2VyLmlkfT5cbiAgICAgICAgPGg1PiBcbiAgICAgICAgICAgIHtwcm9wcy51c2VyLm5vbWJyZX1cbiAgICAgICAgPC9oNT5cbiAgICA8L2Rpdj4pXG59XG5leHBvcnQgZGVmYXVsdCBVc2VyOyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./components/user.js\n");

/***/ }),

/***/ "./components/users.js":
/*!*****************************!*\
  !*** ./components/users.js ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user */ \"./components/user.js\");\n\nvar _jsxFileName = \"/home/manny/Escritorio/foro-next-express-frontend/components/users.js\";\n\n\nconst Users = props => {\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"ul\", {\n    children: props.users.map(user => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"li\", {\n      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_user__WEBPACK_IMPORTED_MODULE_1__.default, {\n        user: user\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 8,\n        columnNumber: 24\n      }, undefined)\n    }, user.id, false, {\n      fileName: _jsxFileName,\n      lineNumber: 7,\n      columnNumber: 20\n    }, undefined))\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 4,\n    columnNumber: 9\n  }, undefined);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Users);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mb3JvLW5leHQtZXhwcmVzcy1mcm9udGVuZC8uL2NvbXBvbmVudHMvdXNlcnMuanM/NGU1ZSJdLCJuYW1lcyI6WyJVc2VycyIsInByb3BzIiwidXNlcnMiLCJtYXAiLCJ1c2VyIiwiaWQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOztBQUNBLE1BQU1BLEtBQUssR0FBRUMsS0FBRCxJQUFTO0FBQ2pCLHNCQUNJO0FBQUEsY0FFUUEsS0FBSyxDQUFDQyxLQUFOLENBQVlDLEdBQVosQ0FBZ0JDLElBQUksaUJBQ2pCO0FBQUEsNkJBQ0ksOERBQUMsMENBQUQ7QUFBTSxZQUFJLEVBQUVBO0FBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKLE9BQVNBLElBQUksQ0FBQ0MsRUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURIO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKO0FBWUgsQ0FiRDs7QUFjQSwrREFBZUwsS0FBZiIsImZpbGUiOiIuL2NvbXBvbmVudHMvdXNlcnMuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVXNlciBmcm9tICcuL3VzZXInXG5jb25zdCBVc2Vycz0ocHJvcHMpPT57XG4gICAgcmV0dXJuKFxuICAgICAgICA8dWw+XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcHJvcHMudXNlcnMubWFwKHVzZXI9PihcbiAgICAgICAgICAgICAgICAgICA8bGkga2V5PXt1c2VyLmlkfT5cbiAgICAgICAgICAgICAgICAgICAgICAgPFVzZXIgdXNlcj17dXNlcn0vPlxuICAgICAgICAgICAgICAgICAgIDwvbGk+ICBcbiAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICB9XG4gICAgICAgIDwvdWw+XG4gICAgKVxufVxuZXhwb3J0IGRlZmF1bHQgVXNlcnM7Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./components/users.js\n");

/***/ }),

/***/ "./pages/u/all.js":
/*!************************!*\
  !*** ./pages/u/all.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _components_users__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/users */ \"./components/users.js\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);\n\nvar _jsxFileName = \"/home/manny/Escritorio/foro-next-express-frontend/pages/u/all.js\";\n\n\n\n\nconst UsersList = props => {\n  const {\n    0: users,\n    1: setUsers\n  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)([]);\n  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {\n    const getToken = () => localStorage.getItem('token');\n\n    const token = getToken();\n    console.log('token', token);\n\n    const getUsers = async token => {\n      if (token) {\n        const res = await axios__WEBPACK_IMPORTED_MODULE_2___default().get('http://localhost:3000/api/users', {\n          headers: {\n            'x-access-token': token\n          }\n        });\n\n        if (res.data) {\n          console.log('entree');\n          setUsers(res.data);\n        }\n      }\n    };\n\n    getUsers(token);\n  }, []);\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n      children: \"users\"\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 30,\n      columnNumber: 12\n    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"ul\", {\n      children: users && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_users__WEBPACK_IMPORTED_MODULE_1__.default, {\n        users: users\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 34,\n        columnNumber: 19\n      }, undefined)\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 31,\n      columnNumber: 12\n    }, undefined)]\n  }, void 0, true, {\n    fileName: _jsxFileName,\n    lineNumber: 29,\n    columnNumber: 8\n  }, undefined);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (UsersList);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mb3JvLW5leHQtZXhwcmVzcy1mcm9udGVuZC8uL3BhZ2VzL3UvYWxsLmpzPzUyYzEiXSwibmFtZXMiOlsiVXNlcnNMaXN0IiwicHJvcHMiLCJ1c2VycyIsInNldFVzZXJzIiwidXNlU3RhdGUiLCJ1c2VFZmZlY3QiLCJnZXRUb2tlbiIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJ0b2tlbiIsImNvbnNvbGUiLCJsb2ciLCJnZXRVc2VycyIsInJlcyIsImF4aW9zIiwiaGVhZGVycyIsImRhdGEiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7O0FBQ0csTUFBTUEsU0FBUyxHQUFFQyxLQUFELElBQVM7QUFDckIsUUFBTTtBQUFBLE9BQUNDLEtBQUQ7QUFBQSxPQUFPQztBQUFQLE1BQWlCQywrQ0FBUSxDQUFDLEVBQUQsQ0FBL0I7QUFDQUMsa0RBQVMsQ0FBQyxNQUFJO0FBQ2IsVUFBTUMsUUFBUSxHQUFDLE1BQ1hDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQURKOztBQUdBLFVBQU1DLEtBQUssR0FBQ0gsUUFBUSxFQUFwQjtBQUNBSSxXQUFPLENBQUNDLEdBQVIsQ0FBWSxPQUFaLEVBQW9CRixLQUFwQjs7QUFDRyxVQUFNRyxRQUFRLEdBQUMsTUFBTUgsS0FBTixJQUFjO0FBQ3pCLFVBQUdBLEtBQUgsRUFBUztBQUNKLGNBQU1JLEdBQUcsR0FBQyxNQUFNQyxnREFBQSxDQUFVLGlDQUFWLEVBQTRDO0FBQUNDLGlCQUFPLEVBQUM7QUFDakUsOEJBQWlCTjtBQURnRDtBQUFULFNBQTVDLENBQWhCOztBQUdBLFlBQUdJLEdBQUcsQ0FBQ0csSUFBUCxFQUFZO0FBQ1JOLGlCQUFPLENBQUNDLEdBQVIsQ0FBWSxRQUFaO0FBQ0FSLGtCQUFRLENBQUNVLEdBQUcsQ0FBQ0csSUFBTCxDQUFSO0FBQ0g7QUFFSjtBQUVMLEtBWkQ7O0FBYUFKLFlBQVEsQ0FBQ0gsS0FBRCxDQUFSO0FBRUgsR0FyQlEsRUFxQlAsRUFyQk8sQ0FBVDtBQXNCSCxzQkFDRztBQUFBLDRCQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURKLGVBRUk7QUFBQSxnQkFFT1AsS0FBSyxpQkFDTCw4REFBQyxzREFBRDtBQUFPLGFBQUssRUFBRUE7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSFA7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFESDtBQVlILENBcENFOztBQXNDSCwrREFBZUYsU0FBZiIsImZpbGUiOiIuL3BhZ2VzL3UvYWxsLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXJzIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvdXNlcnMnXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQge3VzZVN0YXRlLCB1c2VFZmZlY3R9IGZyb20gJ3JlYWN0J1xuICAgY29uc3QgVXNlcnNMaXN0PShwcm9wcyk9PnsgXG4gICAgICAgY29uc3QgW3VzZXJzLHNldFVzZXJzXT11c2VTdGF0ZShbXSlcbiAgICAgICB1c2VFZmZlY3QoKCk9PntcbiAgICAgICAgY29uc3QgZ2V0VG9rZW49KCk9PlxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJylcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHRva2VuPWdldFRva2VuKClcbiAgICAgICAgY29uc29sZS5sb2coJ3Rva2VuJyx0b2tlbik7XG4gICAgICAgICAgIGNvbnN0IGdldFVzZXJzPWFzeW5jKHRva2VuKT0+e1xuICAgICAgICAgICAgICAgaWYodG9rZW4pe1xuICAgICAgICAgICAgICAgICAgICBjb25zdCByZXM9YXdhaXQgYXhpb3MuZ2V0KCdodHRwOi8vbG9jYWxob3N0OjMwMDAvYXBpL3VzZXJzJyx7aGVhZGVyczp7XG4gICAgICAgICAgICAgICAgICAgICAgICAneC1hY2Nlc3MtdG9rZW4nOnRva2VuXG4gICAgICAgICAgICAgICAgICAgIH19KVxuICAgICAgICAgICAgICAgICAgICBpZihyZXMuZGF0YSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZW50cmVlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRVc2VycyhyZXMuZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgXG4gICAgICAgICAgIH1cbiAgICAgICAgICAgZ2V0VXNlcnModG9rZW4pXG4gICAgICAgICAgIFxuICAgICAgIH0sW10pXG4gICAgcmV0dXJuIChcbiAgICAgICA8ZGl2PlxuICAgICAgICAgICA8aDE+dXNlcnM8L2gxPlxuICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHVzZXJzICYmXG4gICAgICAgICAgICAgICAgICA8VXNlcnMgdXNlcnM9e3VzZXJzfS8+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgPC91bD5cbiAgICAgICA8L2Rpdj5cbiAgICApXG4gICAgXG59XG5cbmV4cG9ydCBkZWZhdWx0IFVzZXJzTGlzdDsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/u/all.js\n");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/u/all.js"));
module.exports = __webpack_exports__;

})();