import Head from 'next/head'
import Container from '../../components/container'
import Post from '../../components/post'
const PostById=(props)=>{
    return(
        
        <Container>
            <Head><title>posts</title></Head>
            {<Post post={props.post}/>}
        </Container>
    )
}
PostById.getInitialProps=async (ctx)=>{
    const response=await fetch('http://localhost:3000/api/post/'+ctx.query.id)
    const resJSON=await response.json()
    return {post:resJSON}
}
export default PostById