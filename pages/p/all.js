import Container from '../../components/container'
import Posts from '../../components/posts'
import fetch from 'isomorphic-fetch'
import Head from 'next/head'
const ListPosts=(props)=>{
    return (
        <Container>
            <Head><title>posts</title></Head>
            <Posts posts={props.posts}/>
        </Container>
    )
}
ListPosts.getInitialProps=  async (ctx)=>{
    const response = await fetch('http://localhost:3000/api/posts')
    const resJSON= await response.json()
    return {posts:resJSON}
}
export default ListPosts