import Container from '../components/container'
import Navigation from '../components/navigation'
import Head from 'next/head'
import ListPosts from './p/all'
import Posts from '../components/posts'
const Index=(props)=>{
    return (
        <Container>
        <div>
            <Head>
                <title>inicio</title>
            </Head>
        <Navigation></Navigation>
            
            <Posts posts={props.posts}/>
        </div>
        </Container>
       
    )
}
Index.getInitialProps=async(ctx)=>{
    const res= await ListPosts.getInitialProps(ctx)
    return {posts:res.posts}
}
export default Index;