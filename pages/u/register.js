import React from 'react'
import RegisterComponent from '../../components/register'
import Container from '../../components/container'
import Navigation from '../../components/navigation'
function Register() {
    return (
        <Container>
            <Navigation/>
            <div className="p-2">
            <RegisterComponent/>
            </div>
        </Container>
            
    
    )
}

export default Register
