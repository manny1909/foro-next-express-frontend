import Users from '../../components/users'
import axios from 'axios'
import {useState, useEffect} from 'react'
   const UsersList=(props)=>{ 
       const [users,setUsers]=useState([])
       useEffect(()=>{
        const getToken=()=>
            localStorage.getItem('token')
        
        const token=getToken()
        console.log('token',token);
           const getUsers=async(token)=>{
               if(token){
                    const res=await axios.get('http://localhost:3000/api/users',{headers:{
                        'x-access-token':token
                    }})
                    if(res.data){
                        console.log('entree');
                        setUsers(res.data)
                    }

                }
               
           }
           getUsers(token)
           
       },[])
    return (
       <div>
           <h1>users</h1>
           <ul>
              {
                  users &&
                  <Users users={users}/>
              }
           </ul>
       </div>
    )
    
}

export default UsersList;