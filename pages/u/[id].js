import User from '../../components/user'
import fetch from 'isomorphic-fetch'
const UserProfile=(props)=>{
    return (<div>
        {<User user={props.user}/>}
    </div>)
}
UserProfile.getInitialProps=async (ctx)=>{
    const response=await fetch('http://localhost:3000/api/user/'+ctx.query.id)
    const resJSON=await response.json();
    console.log(resJSON);
    return {user:resJSON}
}
export default UserProfile;